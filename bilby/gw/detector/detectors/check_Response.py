import bilby
import numpy as np
ra    = np.pi
dec   = np.pi/3
psi   = np.pi/4
geocent_time = 1247227950.

interferometers = bilby.gw.detector.InterferometerList(['CE', 'I1', 'K1', 'ET1', 'ET2', 'ET3'])
ce = interferometers[0]
i1 = interferometers[1]
k1 = interferometers[2]
et1 = interferometers[3]
et2 = interferometers[4]
et3 = interferometers[5]

print('CE, Fp:', ce.antenna_response(ra,dec,geocent_time,psi,'plus'))
print('CE, Fc:', ce.antenna_response(ra,dec,geocent_time,psi,'cross'))
print('I1, Fp:', i1.antenna_response(ra,dec,geocent_time,psi,'plus'))
print('I1, Fc:', i1.antenna_response(ra,dec,geocent_time,psi,'cross'))
print('K1, Fp:', k1.antenna_response(ra,dec,geocent_time,psi,'plus'))
print('K1, Fc:', k1.antenna_response(ra,dec,geocent_time,psi,'cross'))


print('ET1, Fp:', et1.antenna_response(ra,dec,geocent_time,psi,'plus'))
print('ET1, Fc:', et1.antenna_response(ra,dec,geocent_time,psi,'cross'))
print('ET2, Fp:', et2.antenna_response(ra,dec,geocent_time,psi,'plus'))
print('ET2, Fc:', et2.antenna_response(ra,dec,geocent_time,psi,'cross'))
print('ET3, Fp:', et3.antenna_response(ra,dec,geocent_time,psi,'plus'))
print('ET3, Fc:', et3.antenna_response(ra,dec,geocent_time,psi,'cross'))

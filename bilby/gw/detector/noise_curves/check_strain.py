import numpy as np
from matplotlib import use
from matplotlib import pyplot as plt
use('agg')
data = np.genfromtxt("ce2_20km_cb_asd.txt", unpack=True)
out = np.array([data[0], data[1]**2])
data2 = np.genfromtxt("ce2_40km_cb.txt", unpack=True)
np.savetxt("ce2_20km_cb.txt", out.T)
plt.plot(out[0], np.sqrt(out[1]))
plt.plot(data2[0], np.sqrt(data2[1]))
plt.yscale('log')
plt.xscale('log')
plt.ylim(1e-25, 7e-23)
plt.xlim(3,8192)
plt.grid(True, which="both")
plt.savefig("/home/kwan-yeung.ng/public_html/PBH_PopIII/ce_curves.pdf")
plt.close()
